require 'rails_helper'

feature 'user creates new category' do
  scenario 'successfully' do
    category_name = 'Tecnologia'

    visit new_category_path
    fill_in 'Nome', with: category_name
    click_on 'Criar Categoria'

    expect(page).to have_css('h1', text: category_name)
  end

  scenario 'should fill name field' do
    visit new_category_path
    click_on 'Criar Categoria'

    expect(page).to have_content 'Não foi possível criar a categoria'
  end

  scenario 'should not allow duplicates' do
    category_name = 'Tecnologia'
    Category.create name: category_name

    visit new_category_path
    fill_in 'Nome', with: category_name
    click_on 'Criar Categoria'

    expect(page).to have_content 'Não foi possível criar a categoria'
  end
end
