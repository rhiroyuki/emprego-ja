class CategoriesController < ApplicationController
  def show
    @category = Category.find params[:id]
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new category_params

    if @category.save
      flash[:success] = 'Sucesso!'
      redirect_to @category
    else
      flash.now[:alert] = 'Não foi possível criar a categoria'
      render 'new'
    end
  end

  private
    def category_params
      params.require(:category).permit(:name)
    end
end
