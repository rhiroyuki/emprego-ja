Rails.application.routes.draw do
  root "homepage#index"
  resources :categories, only: [:show, :new, :create]
  resources :jobs, only: [:show, :new, :create, :edit, :update]
  resources :companies, only: [:new, :create, :show]
end
