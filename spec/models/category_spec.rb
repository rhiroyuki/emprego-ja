require 'rails_helper'

RSpec.describe Category, type: :model do
  it 'should be valid' do
    category = Category.create

    expect(category).to_not be_valid
    expect(category.errors[:name]).to include "can't be blank"
  end

  it 'should not accept duplicates' do
    category_name = 'Tecnologia'
    Category.create name: category_name
    category = Category.create name:category_name

    expect(category).to_not be_valid
    expect(category.errors[:name]).to include 'has already been taken'
  end
end
