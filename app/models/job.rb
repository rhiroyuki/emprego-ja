class Job < ApplicationRecord
  DAYS_TO_EXPIRE = 90
  DAYS_TO_BE_RECENT = 5
  belongs_to :company
  belongs_to :category

  validates :title, :location, :description, :category, presence: true

  def recent?
    (Time.now.to_date  - created_at.to_date).to_i <= DAYS_TO_BE_RECENT
  end

  def expired?
    (Time.now.to_date  - created_at.to_date).to_i >= DAYS_TO_EXPIRE
  end
end
