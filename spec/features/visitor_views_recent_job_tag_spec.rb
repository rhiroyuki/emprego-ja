require 'rails_helper'

feature 'Visitor views a flag indicating recent jobs' do
  scenario 'successfully' do
    company = Company.create(name: 'Campus Code',
                              location: 'São Paulo',
                              phone: '11 2369 3476',
                              mail: 'contato@campuscode.com.br')

    category = Category.create(name: 'Desenvolvedor')

    job = company.jobs.create(title: 'Desenvolvedor Rails',
                         description: 'Desenvolvedor Full Stack Rails',
                         location: 'São Paulo - SP',
                         category: category)

    visit job_path job

    expect(page).to have_xpath("//img[contains(@src,'new')]")
  end

  scenario 'old job' do
    company = Company.create(name: 'Campus Code',
                              location: 'São Paulo',
                              phone: '11 2369 3476',
                              mail: 'contato@campuscode.com.br')

    category = Category.create(name: 'Desenvolvedor')

    travel_to 6.days.ago do
      @job = company.jobs.create(title: 'Desenvolvedor Rails',
                           description: 'Desenvolvedor Full Stack Rails',
                           location: 'São Paulo - SP',
                           category: category)
    end

    visit job_path @job

    expect(page).to_not have_xpath("//img[contains(@src,'new')]")
  end
end
